package testhelper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

public class Assertions {

	public void compareJsonFiles(String expectedFile, String actualFile) throws IOException, JSONException {

		Path expectedFilePath = Paths.get(expectedFile);
		Path actualFilePath = Paths.get(actualFile);
		String expectedJson = new String(Files.readAllBytes(expectedFilePath));
		String actualJson = new String(Files.readAllBytes(actualFilePath));

		JSONAssert.assertEquals(expectedJson, actualJson, JSONCompareMode.STRICT);
	}
	
	public <T> boolean compareObjects(T actualOutput, T expectedOutput) {
		boolean isEqual = false;

		if(Assertions.getValue(actualOutput, "name").equals(Assertions.getValue(expectedOutput, "name"))) {
			if(Assertions.getValue(actualOutput, "designation").equals(Assertions.getValue(expectedOutput, "designation"))) {
				isEqual = true;
			}
		}
		
		return isEqual;
	}
		
		private static <T> String getValue(T object, String property) {
			String idValue = null;
			try {
				idValue = object.getClass().getDeclaredField(property).get(object).toString();
			} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			}
			return idValue;
		}
		
}
