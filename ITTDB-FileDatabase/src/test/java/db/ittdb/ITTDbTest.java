package db.ittdb;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import db.exceptions.DbUpdateException;
import db.exceptions.ObjectNotFoundException;
import testhelper.Assertions;
import testhelper.Employee;

public class ITTDbTest {
	Db db;
	Employee emp;
	Assertions assertion ;
	String expectedFilePath, actualFilePath;
	boolean isSame;
	
	@BeforeClass
	public void projectSetup() {
		db = new ITTDb();
		emp = new Employee();
		assertion = new Assertions();
		expectedFilePath = "src/test/resources/Employee.json";
		actualFilePath = "FileStorage/Employee.json";
	}
	@Test(priority = 1)
	public void save_nonNullObject_shouldGetSaved() throws IOException, JSONException {
		emp.setName("deekshitha");
		emp.setDesignation("manager");
		db.save(emp, "Employee");
		assertion.compareJsonFiles(expectedFilePath, actualFilePath);
	}
	
//	@Test(priority = 2)
//	public void search_existingObject_shouldReturnValidData() throws JSONException, ObjectNotFoundException {
//		List<Employee> searchedEmpList = new ArrayList();
//		searchedEmpList = db.search("name", "deekshitha", emp);
//		boolean isFound = assertion.compareObjects(searchedEmpList.get(0), emp);
//		assertTrue(isFound);
//	}
	
//	@Test(priority = 3, expectedExceptions = { ObjectNotFoundException.class })
//	public void search_nonExistingObject_shouldNotReturnData() throws JSONException, ObjectNotFoundException {
//		List<Employee> searchedEmpList = new ArrayList();
//		searchedEmpList = db.search("name", "inTime", emp);
//	}
//	
//	@Test(priority = 4)
//	public void update_existingObject_shouldUpdateObject() throws DbUpdateException {
//		Employee emp3 = new Employee();
//		emp3.setName("shubham");
//		emp3.setDesignation("manager");
//		boolean isUpdated = db.update(1, emp3);
//		assertTrue(isUpdated);
//	}
//	
//	@Test(priority = 5)
//	public void delete_existingObject_shouldDeleteObject() throws ObjectNotFoundException {
//		Employee emp = new Employee();
//		boolean isDeleted = db.delete("name", "shubham", emp);
//		assertTrue(isDeleted);
//	}
//	
}
