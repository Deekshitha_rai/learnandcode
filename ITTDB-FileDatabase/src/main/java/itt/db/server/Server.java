package itt.db.server;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import db.exceptions.ObjectNotFoundException;
import db.ittdb.Db;
import db.ittdb.ITTDb;

public class Server {
	public static void main(String args[]) throws IOException, ObjectNotFoundException {
	
		DbServer server = new DbServer();
		server.start();
		
	}
}
