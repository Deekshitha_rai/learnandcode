package itt.db.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import db.exceptions.DbUpdateException;
import db.exceptions.ObjectNotFoundException;
import db.ittdb.Db;
import db.ittdb.ITTDb;
import db.serializers.JSONSerializer;
import db.serializers.Serializer;
import itt.db.commonLibrary.DbRequest;
import itt.db.commonLibrary.DbResponse;

public class RequestProcessor {

	Db db = new ITTDb();
	DbResponse dbResponse;
	List<Object> searchedObjectList = new ArrayList<Object>();
	LinkedHashMap<Object, Object> dataObject = new LinkedHashMap<Object, Object>();
	static Serializer serializer = new JSONSerializer();

	public DbResponse save(DbRequest dbRequest) throws IOException {
		int savedObjectId = db.save(dbRequest.data, dbRequest.fileName);
		return this.createResponse(savedObjectId, "save");
	}

	public DbResponse search(DbRequest dbRequest) throws ObjectNotFoundException {
		searchedObjectList = db.search(dbRequest.data, dbRequest.fileName);
		if (searchedObjectList.size() != 0) {
			dataObject = (LinkedHashMap<Object, Object>)searchedObjectList.get(0);
			int searchedObjectId = new Integer(dataObject.get("id").toString());
			return new DbResponse("Success", 200, "", searchedObjectList.get(0), searchedObjectId);
		}
		return new DbResponse("Error", 404, "No such record found", "", 0);
	}

	public DbResponse modify(DbRequest dbRequest) throws DbUpdateException {
		int updatedObjectId = db.update(dbRequest.data, dbRequest.fileName);
		return this.createResponse(updatedObjectId, "update");
	}

	public DbResponse delete(DbRequest dbRequest) throws ObjectNotFoundException {
		int deletedObjectId = db.delete(dbRequest.data, dbRequest.fileName);
		return this.createResponse(deletedObjectId, "delete");
	}
	
	private DbResponse createResponse(int objectId, String operation) {
		if (objectId != 0) {
			return new DbResponse("Success", 200, "", "", objectId);
		}
		return new DbResponse("Error", 404, "Not able to " + operation + " data", "", objectId);
	}
}
