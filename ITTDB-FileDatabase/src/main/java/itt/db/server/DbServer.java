package itt.db.server;
import java.io.*;  
import java.net.*;

import db.exceptions.DbUpdateException;
import db.exceptions.ObjectNotFoundException;
import db.ittdb.Db;
import db.ittdb.ITTDb;
import db.serializers.JSONSerializer;
import db.serializers.Serializer;
import itt.db.commonLibrary.DbRequest;
import itt.db.commonLibrary.DbResponse;
import itt.db.commonLibrary.IOManager;  

public class DbServer extends Thread{  

	private ServerSocket serverSocket;
	private Socket socket;
	private DataInputStream dis;
	private DataOutputStream dout;
	IOManager ioManager = new IOManager(); 
	Serializer serializer = new JSONSerializer();
	RequestProcessor requestProcessor = new RequestProcessor();
	
	private void startServer() throws IOException, ObjectNotFoundException, DbUpdateException {
		createServer();
		receiveRequest();
	}

	private void createServer() {
		try {
			serverSocket = new ServerSocket(6666);
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	private void receiveRequest() throws IOException, ObjectNotFoundException, DbUpdateException {
		while(true) {
			try {
				socket = serverSocket.accept();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Creating new connection thread for " + socket.getRemoteSocketAddress());
			processRequest();
		}
	}

	private void processRequest() throws IOException, ObjectNotFoundException, DbUpdateException {
		DbResponse dbResponse = new DbResponse();
		String data = ioManager.readDataFromInputStream(socket);
		DbRequest dbRequest = new DbRequest(); 
		dbRequest = serializer.getJsonObjectFromString(data, dbRequest.getClass());
	
		switch (dbRequest.dbOperation) {
		case "save":
			System.out.println("Request for save Operation");
			dbResponse = requestProcessor.save(dbRequest);
			break;

		case "search":
			System.out.println("Request for search Operation");
			dbResponse = requestProcessor.search(dbRequest);
			break;
			
		case "delete":
			System.out.println("Request for delete Operation");
			dbResponse = requestProcessor.delete(dbRequest);
			break;
			
		case "update":
			System.out.println("Request for update Operation");
			dbResponse = requestProcessor.modify(dbRequest);
			break;
		
		default:
			break;
		}
		sendResponse(dbResponse);
	}

	public void sendResponse(DbResponse dbResponse) {
		String response = serializer.getJsonStringFromObject(dbResponse);
		ioManager.sendDataToOutputStream(socket, response);
	}


	public void stopServer() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			startServer();
			receiveRequest();	
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ObjectNotFoundException e) {
			e.printStackTrace();
		} catch (DbUpdateException e) {
			e.printStackTrace();
		}	   
	}

}  