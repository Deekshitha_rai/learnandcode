package itt.db.commonLibrary;

import org.json.simple.JSONObject;

public class DbRequest {

	public String dbOperation;
	public Object data;
	public String fileName;
	
	public DbRequest() {
		
	}
	public <T> DbRequest(String dbOperation, Object data, String fileName) {
		this.dbOperation = dbOperation;
		this.data = data;
		this.fileName = fileName;
	}
	
	public static Object createObject(Object keyValue, Object fieldName) {
		JSONObject object = new JSONObject();
		object.put("fieldName", fieldName);
		object.put("keyValue", keyValue);
		return object;
	}
	
	public static Object createUpdateObject(int id, Object updatedObject) {
		JSONObject object = new JSONObject();
		object.put("id", id);
		object.put("updatedObject", updatedObject);
		return object;
	}
}
