package itt.db.commonLibrary;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class IOManager {

	public String readDataFromInputStream(Socket socket) {
		DataInputStream dataInputStream;
		String data = "";
		try {
			dataInputStream = new DataInputStream(socket.getInputStream());
			data = dataInputStream.readUTF();
		} catch (IOException e) {
			e.printStackTrace();
		}  
		return data;
	}
	
	public void sendDataToOutputStream(Socket socket, String message) {
		DataOutputStream dataOutputStream;
		try {
			dataOutputStream = new DataOutputStream(socket.getOutputStream());
			dataOutputStream.writeUTF(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
