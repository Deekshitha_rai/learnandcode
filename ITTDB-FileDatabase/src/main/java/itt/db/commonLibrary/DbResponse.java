package itt.db.commonLibrary;

import org.json.simple.JSONObject;

public class DbResponse {

	public String status;
	public int statusCode;
	public String errorMessage;
	public Object data;
	public int id;
	
	public DbResponse() {
		
	}
	
	public <T> DbResponse(String status, int statusCode, String errorMsg, Object data, int objectId) {
		this.status = status;
		this.statusCode = statusCode;
		this.errorMessage = errorMsg;
		this.data = data;
		this.id = objectId;
	}
	
	public static Object createResponseDataObject(int objectId) {
		JSONObject object = new JSONObject();
		object.put("id", objectId);
		return object;
	}
}
