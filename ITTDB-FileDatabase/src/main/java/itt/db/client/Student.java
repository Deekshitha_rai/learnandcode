package itt.db.client;

public class Student {

	@Override
	public String toString() {
		return " id=" + id + ", name=" + name + ", age=" + age;
	}

	public int id;
	public String name;
	public int age;

	public Student() {

	}
	
	public Student(String name, int age) {
		this.age = age;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
