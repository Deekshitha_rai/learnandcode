package itt.db.client;


public class Employee {
	@Override
	public String toString() {
		return " id=" + id + ", name=" + name + ", designation=" + designation;
	}

	public int id;
	public String name;
	public String designation;

	public Employee() {

	}
	
	public Employee(String name, String designation) {
		this.designation = designation;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
}
