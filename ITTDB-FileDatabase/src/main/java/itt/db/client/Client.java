package itt.db.client;
import java.io.IOException;
import java.net.UnknownHostException;

import db.serializers.JSONSerializer;
import db.serializers.Serializer;

public class Client {
	public static void main(String[] args) throws UnknownHostException, IOException {

		RequestHandler client = new RequestHandler();
		
		Employee emp = new Employee("harry", "manager");
		
//		client.search("name", "deek", emp);
		
//		client.delete("name", "nju", emp);
		
//		client.modify(2, emp);
			
//		client.save(emp);

		client.receiveResponseFromServer();
	}
}
