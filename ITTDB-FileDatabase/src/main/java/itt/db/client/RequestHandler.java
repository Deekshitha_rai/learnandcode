package itt.db.client;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;

import db.serializers.JSONSerializer;
import db.serializers.Serializer;
import itt.db.commonLibrary.DbRequest;
import itt.db.commonLibrary.DbResponse;

public class RequestHandler {

	static ConnectionHandler client ;
	static Serializer serializer = new JSONSerializer();
	static LinkedHashMap<Object, Object> dataObject = new LinkedHashMap<Object, Object>();
	
	public RequestHandler() throws UnknownHostException, IOException {
		client = new ConnectionHandler("localhost", 6666);
		client.createConnection();
	}
	
	public static void save(Object saveObject) {
		DbRequest dbRequest = new DbRequest("save", saveObject, saveObject.getClass().getSimpleName());
		sendRequestToServer(dbRequest);
	}
	
	public static void search(String fieldName, String keyValue, Object searchObject) {
		String fileName = searchObject.getClass().getSimpleName();
		DbRequest dbRequest = new DbRequest("search", DbRequest.createObject(keyValue, fieldName), fileName);
		sendRequestToServer(dbRequest);
	}
	
	public static void delete(String fieldName, String keyValue, Object searchObject) {
		String fileName = searchObject.getClass().getSimpleName();
		DbRequest dbRequest = new DbRequest("delete", DbRequest.createObject(keyValue, fieldName), fileName);
		sendRequestToServer(dbRequest);
	}
	
	public static void modify(int id, Object updateObject) {
		String fileName = updateObject.getClass().getSimpleName();
		DbRequest dbRequest = new DbRequest("update", DbRequest.createUpdateObject(id, updateObject), fileName);
		sendRequestToServer(dbRequest);
	}
	
	private static void sendRequestToServer(DbRequest dbRequest) {
		String request = serializer.getJsonStringFromObject(dbRequest);
		client.sendRequest(request);
	}
	
	public static void receiveResponseFromServer() {
		DbResponse response = new DbResponse();
		String serverResponse = client.receiveResponse();
		response = serializer.getJsonObjectFromString(serverResponse, response.getClass());
		processResponse(response);
	}
	
	private static void processResponse(DbResponse dbResponse) {
		if (dbResponse.statusCode == 200) {
			System.out.println(dbResponse.status + " with status code " + dbResponse.statusCode); 
			if (dbResponse.data != "") {
				dataObject = (LinkedHashMap<Object, Object>) dbResponse.data;
				System.out.println("Search Result :");
				System.out.println("Id          - " + dataObject.get("id").toString());
				System.out.println("Name        - " + dataObject.get("name").toString());
				System.out.println("Designation - " + dataObject.get("designation").toString());
			} else {
				System.out.println(" for Object Id " + dbResponse.id);
			}
			return ;
		}
		System.out.println(dbResponse.status + " with status code " + dbResponse.statusCode);
		System.out.println("Error Message = " + dbResponse.errorMessage);
	}
}
