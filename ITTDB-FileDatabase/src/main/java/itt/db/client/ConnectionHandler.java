package itt.db.client;
import java.io.*;  
import java.net.*;

import itt.db.commonLibrary.IOManager; 

public class ConnectionHandler {  

	private Socket socket;
	private DataOutputStream dataOutputStream;
	private DataInputStream dataInputStream;
	IOManager ioManager; 

	private String hostName;
	private int port;

	public ConnectionHandler(String hostName, int port) {
		this.hostName = hostName;
		this.port = port;
		ioManager = new IOManager();
	}

	public void createConnection() throws UnknownHostException, IOException {
		socket = new Socket(this.hostName, this.port);
	}
	
	public void sendRequest(String request) {
		ioManager.sendDataToOutputStream(socket, request);  
	}
	
	public String receiveResponse() {
		return ioManager.readDataFromInputStream(socket); 
		
	}
	
	public void closeConnection() {
		try {
			dataOutputStream.close();
			dataOutputStream.flush();
			socket.close();  
		} catch (IOException e) {
			e.printStackTrace();
		}  
	}

}  