package db.serializers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONSerializer implements Serializer{
	private ObjectMapper objectMapper = new ObjectMapper();
	private JsonGenerator jsonGenerator;
	private JsonParser jsonParser;

	public <T> void serialize(String filePath, List<T> objectList) {
		FileOutputStream outStream;
		try {
			outStream = new FileOutputStream(filePath);
			jsonGenerator = objectMapper.getFactory().createGenerator(outStream);
			objectMapper.writeValue(jsonGenerator, objectList);
		}catch (IOException e) {
		
		}
	}

	public <T> List<T> deserialize(String filePath, Class<T> type) {
		FileInputStream inStream;
		List<T> objectList = null;
		try {
			inStream = new FileInputStream(filePath);
			jsonParser = objectMapper.getFactory().createParser(inStream);
			objectList = objectMapper.readValue(jsonParser, objectMapper.getTypeFactory().constructCollectionType(List.class, type));
		} catch (IOException e) {

		}
		return objectList;
	}
	
	public <T> String getJsonStringFromObject(T object){
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonString;
	}

	public <T> T getJsonObjectFromString(String string, Class<T> type){
		T object = null;
		try {
			object = objectMapper.readValue(string, type);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return object;
	}
}
