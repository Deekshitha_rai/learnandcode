package db.serializers;

import java.util.List;

public interface Serializer {
	public <T> void serialize(String filePath, List<T> objectList);
	public <T> List<T> deserialize(String filePath, Class<T> type);
	public <T> String getJsonStringFromObject(T object);
	public <T> T getJsonObjectFromString(String string, Class<T> type);
}
