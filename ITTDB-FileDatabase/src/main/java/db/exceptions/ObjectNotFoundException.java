package db.exceptions;

public class ObjectNotFoundException extends Exception {
	
	private String errorMessage;

	public ObjectNotFoundException(String errorMessage) {
		super(errorMessage);
	}
}
