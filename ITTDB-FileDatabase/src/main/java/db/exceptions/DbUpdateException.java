package db.exceptions;

public class DbUpdateException extends Exception{

	private String errorMessage;

	public DbUpdateException(String errorMessage) {
		super(errorMessage);
	}
	
}
