package db.ittdb;

import java.io.IOException;
import java.util.List;

import db.exceptions.DbUpdateException;
import db.exceptions.ObjectNotFoundException;

public interface Db {
	public int save(Object Object, String fileName)throws IOException;
	public List<Object> search(Object object, String fileName) throws ObjectNotFoundException;
	public int delete(Object object, String fileName) throws ObjectNotFoundException;
	public int update(Object object, String fileName) throws DbUpdateException;
}
