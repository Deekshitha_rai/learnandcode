package db.ittdb;

import java.io.IOException;
import java.util.List;

import db.exceptions.DbUpdateException;
import db.exceptions.ObjectNotFoundException;
import db.manager.StorageManager;

public class ITTDb implements Db{
	StorageManager storageManager = new StorageManager();

	public int save(Object object, String fileName) throws IOException {
		return storageManager.save(object, fileName);
	}

	public List<Object> search(Object object, String fileName) throws ObjectNotFoundException {
		return storageManager.search(object, fileName);
	}

	public int delete(Object object, String fileName) throws ObjectNotFoundException {
		return storageManager.delete(object, fileName);
	}

	public int update(Object object, String fileName) throws DbUpdateException {
		return storageManager.update(object, fileName);
	}

}
