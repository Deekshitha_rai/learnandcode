package db.manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import db.exceptions.DbUpdateException;
import db.exceptions.ObjectNotFoundException;
import db.serializers.JSONSerializer;
import db.serializers.Serializer;

public class StorageManager {
	static Serializer serializer = new JSONSerializer();
	
	public int save(Object object, String fileName) throws IOException {
		
		List<Object> objectList = new ArrayList<Object>();
		int savedObjectId;
		String filePath = FileManager.filePathGenerator(fileName);
		if(!FileManager.isFileExists(filePath)) {
			FileManager.createFile(filePath);
		}
		else if(!FileManager.isFileEmpty(filePath)) {
			objectList = (List<Object>) serializer.deserialize(filePath, object.getClass());
		}
		savedObjectId = this.generateObjectId(objectList, object);
		objectList = this.addObjectToObjectList(objectList, object);
		serializer.serialize(filePath, objectList);
		return savedObjectId;

	}
	
	public List<Object> search(Object object, String fileName) {
		
		List<Object> objectList = new ArrayList<Object>();
		String filePath = FileManager.filePathGenerator(fileName);
		objectList = (List<Object>) serializer.deserialize(filePath, object.getClass());
		String fieldName = this.getProperty(object, "fieldName");
		String searchValue = this.getProperty(object, "keyValue");
		return this.findTheRequiredRecords(fieldName, searchValue, objectList);
	}
	
	public int delete(Object object, String fileName) {
		
		List<Object> objectList = new ArrayList<Object>();
		String filePath = FileManager.filePathGenerator(fileName);
		objectList = (List<Object>) serializer.deserialize(filePath, object.getClass());
		String deleteKey = this.getProperty(object, "fieldName");
		String deleteValue = this.getProperty(object, "keyValue");
		List<Object> searchedObjects = this.findTheRequiredRecords(deleteKey, deleteValue, objectList);
		if(searchedObjects.size() != 0) {
			objectList.remove(searchedObjects.get(0));
			serializer.serialize(filePath, objectList);
			return new Integer(this.getProperty(searchedObjects.get(0), "id"));
		}
		return 0;
	}
	
	public int update(Object object, String fileName) {
		
		List<Object> objectList = new ArrayList<Object>();
		String filePath = FileManager.filePathGenerator(fileName);
		int id = new Integer(this.getProperty(object, "id"));
		LinkedHashMap<String, Object> tempObject = (LinkedHashMap<String, Object>)object;
		Object updatedObject = tempObject.get("updatedObject");
		objectList = (List<Object>) serializer.deserialize(filePath, object.getClass());
		boolean isUpdated = this.updateMatchedRecords(id, updatedObject, objectList);
		if(isUpdated) {
			serializer.serialize(filePath, objectList);
			return id;
		}
		return 0;
	}
	
	private boolean updateMatchedRecords(int id, Object updateObject, List<Object> objectList) {
		List<Object> matchedRecordList = new ArrayList<Object>();
		boolean isUpdated = false;
		for(int objectIndex = 0; objectIndex < objectList.size(); objectIndex ++) {
			if(this.getProperty(objectList.get(objectIndex), "id").equals(new Integer(id).toString())) {
				this.setId(updateObject, id);
				objectList.set(objectIndex, updateObject);
				isUpdated = true;
			}
			
		}
		return isUpdated;
	}
	
	private List<Object> findTheRequiredRecords(String fieldName, String searchValue, List<Object> objectList) {
		List<Object> matchedRecordList = new ArrayList<Object>();
		String fieldValue;
		for(int index = 0 ; index < objectList.size(); index ++) {
			fieldValue = this.getProperty(objectList.get(index), fieldName);
			if(fieldValue.equals(searchValue)) {
				matchedRecordList.add(objectList.get(index));
			}
		}
		return matchedRecordList;
	}
	
	private List<Object> addObjectToObjectList(List<Object> objectList, Object object){
		objectList.add(object);
		return objectList;
	}
	
	private int generateObjectId(List<Object> objectList, Object object) {
		int lastObjectId;
		if(objectList.size() == 0) {
			return this.setId(object, 1);
		} else {
			lastObjectId = new Integer(this.getProperty(objectList.get(objectList.size()-1), "id"));
			return this.setId(object, lastObjectId + 1);
		}
	}
	
	private String getProperty(Object object, String property) {
		String idValue = null;
		LinkedHashMap<Object, Object> dataObject = new LinkedHashMap<Object, Object>();
		dataObject = (LinkedHashMap<Object, Object>) object;
		idValue = dataObject.get(property).toString();
		return idValue;
	}
	
	private int setId(Object object, int id) {
		LinkedHashMap<Object, Object> dataObject = new LinkedHashMap<Object, Object>();
		dataObject = (LinkedHashMap<Object, Object>) object;
		dataObject.put("id", id);
		return id;
	}
		
}
