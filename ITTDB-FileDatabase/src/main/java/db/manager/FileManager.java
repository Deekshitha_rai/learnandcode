package db.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

public class FileManager {
	
	public static <T> String filePathGenerator(String fileName) {
		return System.getProperty("user.dir") + "//FileStorage//" + fileName +".json";
	}
	
	public static boolean isFileExists(String filePath) {
		return (Files.exists(Paths.get(filePath), LinkOption.NOFOLLOW_LINKS)) ? true : false;
	}
	
	public static void createFile(String filePath) throws IOException {
		File file = new File(filePath);
		file.createNewFile();
	}
	
	public static boolean isFileEmpty(String filePath) {
		boolean isEmpty = false;
		FileInputStream fileInputStream;

		try { 
			fileInputStream = new FileInputStream(filePath);
			isEmpty = (fileInputStream.getChannel().size() == 0) ? true : false;
		} catch (IOException e) {
			
		}
		return isEmpty;
		
	}
}
