import java.io.*;  
import java.net.*;  
public class Server {  
	public static void main(String[] args){  
		try{  
			ServerSocket serverSocket=new ServerSocket(6666); 
 			System.out.println("server listening at port 6666");
			Socket socket=serverSocket.accept();	   
			DataInputStream dataInputStream=new DataInputStream(socket.getInputStream());  
			String  clientData=dataInputStream.readUTF();  
			System.out.println("message= "+clientData);  
			DataOutputStream dataOutputStream=new DataOutputStream(socket.getOutputStream());  
			dataOutputStream.writeUTF("Acknowledged");
			socket.close();  
		}catch(Exception e){System.out.println(e);}  	
	}  
}  
