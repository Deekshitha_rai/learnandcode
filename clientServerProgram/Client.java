import java.io.*;  
import java.net.*;  
public class Client {  
	public static void main(String[] args) {  
		try{      
			Socket socket=new Socket("localhost",6666);  
			DataOutputStream dataOutputStream=new DataOutputStream(socket.getOutputStream());  
			dataOutputStream.writeUTF("Hello Server");
			DataInputStream dataInputStream=new DataInputStream(socket.getInputStream());  
			String  clientData=dataInputStream.readUTF();
			System.out.println("message= "+clientData);	
			dataOutputStream.flush();  
			dataOutputStream.close();  
			socket.close();  
		}catch(Exception e){System.out.println(e);}  
	}  
}  
