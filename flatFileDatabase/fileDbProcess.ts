import fs = require("fs");
import read = require("readline-sync");

export class FileDbProcess {
    obtainReqEmpDetails: (empId, empData, next)=> void;
    acceptDetailsFromUser: (objType, next) => void;
    storeDataToDb: (empDetails, objType, next) => void;
    fetchDetailsToUpdate : (empDetails, objType, next)=> void;
    removeEmpDetailsFromDb :(empDetails, objType, next) => void;
}


FileDbProcess.prototype.obtainReqEmpDetails = (empId, empData, next) => {
    try {
        var empIdValidation = inputValidation(empId);
        if(empIdValidation){
            return next(empIdValidation, null);
        }
        var recordIndex, record, reqDetails;
        var dbRecords = empData.toString().split("\n");
        for (recordIndex = 0; recordIndex < dbRecords.length - 1; recordIndex++) {
            record = JSON.parse(dbRecords[recordIndex]);
            if (record.id == empId) {
                reqDetails = record;
                break;
            }
        }
        if (reqDetails) {
            next(null, reqDetails);
        } else {
            next("The Specified Id is Not Found", null);
        }
    }
    catch(err) {
        next(err, null);
    }
}

FileDbProcess.prototype.storeDataToDb = (empDetails, objType, next) => {
    try {
        var empIdValidation = inputValidation(empDetails.id);
        if(empIdValidation){
            return next(empIdValidation, null);
        }
        empDetails = JSON.stringify(empDetails);
        fs.appendFile(objType + ".txt", "\n" +empDetails, function (err) {
            if (err) {
                next(err, null)
            } else {
                next(null, objType + " Data stored successfully!!")
            }
        });
    }
    catch(err) {
        next(err, null);
    }
}

var acceptDetailsFromUser = module.exports.acceptDetailsFromUser = FileDbProcess.prototype.acceptDetailsFromUser= (objType, next) =>{
    try{
        if (objType == "Manager") {
            var empDetails = new Manager();
        } else {
            var empDetails = new juniorEngineer();
        }
        empDetails.id = read.question("Enter the " + objType + " id:");
        empDetails.name = read.question("Enter the " + objType + " name:");
        empDetails.address = read.question("Enter the " + objType + " address:");
        empDetails.age = read.question("Enter your age:");
        return empDetails;
    }
    catch(err) {
        next(err, null);
    }
}

FileDbProcess.prototype.fetchDetailsToUpdate = (empDetails, objType, next) => {
    try {
        var recordIndex, record, updatedEmpDetails, empIdValidation, modifiedData, dbRecords;
         empIdValidation = inputValidation(empDetails.empId);
        if(empIdValidation){
            return next(empIdValidation, null);
        }
        dbRecords = empDetails.empData.toString().split("\n");
        
        for (recordIndex = 0; recordIndex < dbRecords.length - 1; recordIndex++) {
            record = JSON.parse(dbRecords[recordIndex]);
            if (record.id == empDetails.empId) {
                updatedEmpDetails = acceptDetailsFromUser(objType, next);
                dbRecords.splice(recordIndex, 1, JSON.stringify(updatedEmpDetails));
                storeUpdatedDetails(dbRecords, objType, next);
                break;
            }
        }
        
        if(!updatedEmpDetails) {
            next("No Employee with the specified Id to update", null);
        }
    }
    catch(err) {
        next(err, null);
    }
}


function storeUpdatedDetails(updatedData, objType, next) {
    try {
        var updatedDataOfEmp = updatedData.toString().split("},").join("}\n");
        fs.writeFile(objType + ".txt", updatedDataOfEmp, function (err) {
            if (err) {
                next("failed to store", null);
            } else {
                next(null, objType + " Data updated successfully!!");
            }
        });
    }
    catch(err) {
        next(err, null);
    }
}

FileDbProcess.prototype.removeEmpDetailsFromDb = (empDetails, objType, next) =>{
    try {
        var recordIndex, record, empIdValidation, isEmpIdFound = false;
        empIdValidation = inputValidation(empDetails.empId);
        if(empIdValidation){
            return next(empIdValidation, null);
        }
        var dbRecords = empDetails.empData.toString().split("\n");
        
        for (recordIndex = 0; recordIndex < dbRecords.length - 1; recordIndex++) {
            record = JSON.parse(dbRecords[recordIndex]);
            if (record.id == empDetails.empId) {
                isEmpIdFound = true;
                dbRecords.splice(recordIndex, 1);
                storeUpdatedDetails(dbRecords, objType, next);
                break;
            }
        }
        if(!isEmpIdFound){
            next("No Employee of specified Id to delete", null);
        }
    }
    catch(err) {
        next(err, null);
    }
}

function inputValidation(empId){
    if(!empId){
        return "Employee Id should not be empty";
    }
}

import {Manager} from './managerClass';
import {juniorEngineer} from './junorEngineerClass';