import {FileDbProcess} from "../../flatFileDatabase/fileDbProcess";
import {expect} from "chai";
var fs = require("fs");

describe("StoreDataToDb---->To Store employee Data into File Database", function () {
    it("should store the employee data successfully", function () {
        let fileDbProcess = new FileDbProcess();
        let empDetails = {
            "id" : 66,
            "name" : "rai",
            "address" : "silk board",
            "age" : 22
        }
        let objType = "ManagerMock";

        fileDbProcess.storeDataToDb(empDetails, objType, function (err, data) {
            expect(data).equal("ManagerMock Data stored successfully!!")
            fileDbProcess.obtainReqEmpDetails(empDetails.id, fs.readFileSync("ManagerMock.txt"), function (err, data) {
                expect(data).not.equal(null);
            })
        })
    })

    it("should return employee id should not be empty", function () {
        let fileDbProcess = new FileDbProcess();
        let empDetails = {
            "id" : "",
            "name" : "rai",
        }
        let objType = "ManagerMock";

        fileDbProcess.storeDataToDb(empDetails, objType, function (err, data) {
            expect(err).equal("Employee Id should not be empty");
            expect(data).equal(null)
        })
    })

})