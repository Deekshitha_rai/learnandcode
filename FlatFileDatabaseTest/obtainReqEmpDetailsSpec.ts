import {expect} from "chai";
var fs = require("fs");
import {FileDbProcess} from "../../flatFileDatabase/fileDbProcess";

describe("obtainReqEmpDetails---->To fetch the employee details based on emp id", function () {
    it("should return specified id is not found", function () {
        let fileDbProcess = new FileDbProcess();
        let empId = 3;
        let empData = fs.readFileSync("ManagerMock.txt");

        fileDbProcess.obtainReqEmpDetails(empId, empData, function (err, data) {
            expect(err).equal("The Specified Id is Not Found");
            expect(data).equal(null);
        })
    })

    it("should return employee id should not be empty", function () {
        let fileDbProcess = new FileDbProcess();
        let empId;
        let empData = fs.readFileSync("ManagerMock.txt");

        fileDbProcess.obtainReqEmpDetails(empId, empData, function (err, data) {
            expect(err).equal("Employee Id should not be empty");
            expect(data).equal(null);
        })
    })

    it("should return the employee details of specific Id", function () {
        let fileDbProcess = new FileDbProcess();
        let empId = 4;
        let empData = fs.readFileSync("ManagerMock.txt");

        fileDbProcess.obtainReqEmpDetails(empId, empData, function (err, data) {
            expect(err).equal(null);
            expect(data).not.equal(null);
            expect(data.id).equal("4");
            expect(data.name).equal("this");
            expect(data.address).equal("india");
            expect(data.age).equal("4");
        })
    })
})

