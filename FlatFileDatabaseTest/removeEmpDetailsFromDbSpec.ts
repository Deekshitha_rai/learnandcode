import {FileDbProcess} from "../../flatFileDatabase/fileDbProcess";
import {expect} from "chai";
var fs = require("fs");

describe("removeEmpDetailsFromDb---> Remove The Employee Details based on specific ID", function () {

    before(function () {
        var empDetails = fs.readFileSync("ManagerMock.txt");
        fs.writeFileSync("deleteFileMock.txt", empDetails);
    })

    it("should delete the data successfully", function () {
        let fileDbProcess = new FileDbProcess();
        let empData = fs.readFileSync("deleteFileMock.txt");
        let empDetails = {
            empData : empData,
            empId : 4
        }
        let objType = "deleteFileMock";

        fileDbProcess.removeEmpDetailsFromDb(empDetails, objType, function (err, data) {
          expect(data).equal("deleteFileMock Data updated successfully!!");

        })
    })

    it("should return Employee Id should not be empty", function () {
        let fileDbProcess = new FileDbProcess();
        let empData = fs.readFileSync("deleteFileMock.txt");
        let empDetails = {
            empData : empData
        }
        let objType = "deleteFileMock";

        fileDbProcess.removeEmpDetailsFromDb(empDetails, objType, function (err, data) {
            expect(err).equal("Employee Id should not be empty");
            expect(data).equal(null);
        })
    })

    it("should return No Employee of specified Id to delete", function () {
        let fileDbProcess = new FileDbProcess();
        let empData = fs.readFileSync("deleteFileMock.txt");
        let empDetails = {
            empId : 100,
            empData : empData
        }
        let objType = "deleteFileMock";

        fileDbProcess.removeEmpDetailsFromDb(empDetails, objType, function (err, data) {
            expect(err).equal("No Employee of specified Id to delete");
            expect(data).equal(null);
        })
    })

})