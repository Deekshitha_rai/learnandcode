import {FileDbProcess} from "../../flatFileDatabase/fileDbProcess";
import {expect} from "chai";
var fs = require("fs");

describe("fetchDetailsToUpdate---> Update The Employee Details with new data", function () {

    it("should return Employee Id should not be empty", function () {
        let fileDbProcess = new FileDbProcess();
        let empData = fs.readFileSync("updateFileMock.txt");
        let empDetails = {
            empData : empData
        }
        let objType = "updateFileMock";

        fileDbProcess.fetchDetailsToUpdate(empDetails, objType, function (err, data) {
            expect(err).equal("Employee Id should not be empty");
            expect(data).equal(null);
        })
    })

    it("should return No Employee of specified Id to update", function () {
        let fileDbProcess = new FileDbProcess();
        let empData = fs.readFileSync("updateFileMock.txt");
        let empDetails = {
            empId : 100,
            empData : empData
        }
        let objType = "updateFileMock";

        fileDbProcess.fetchDetailsToUpdate(empDetails, objType, function (err, data) {
            expect(err).equal("No Employee with the specified Id to update");
            expect(data).equal(null);
        })
    })

})